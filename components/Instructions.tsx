export default function Instructions() {
  return (
    <>
      <h1>How to use Date Jar</h1>
      <h3>How does Date Jar work?</h3>
      <p>
        Date Jar uses local storage to store all of your date ideas on the
        device you use. This means you and your partner will have to use ONE
        device to enter in date ideas and you will have to use the same device
        to generate your randomly chosen date.
      </p>
      <h3>Step 1</h3>
      <p>
        Collaborate with your partner in adding a wide variety of dates to the
        Date Jar through the "Add date idea!" form - the more dates the better!
      </p>
      <h3>Step 2</h3>
      <p>
        When it's about that time to go on a date, you can either:
        <p>
          Select your preferences for the date on the home page and click "Lets
          go on a date!" to generate a randomized date with selected preferences
        </p>
        <p>*OR*</p>
        <p>
          Click on "We're feelin' lucky~" to randomly select from ALL dates in
          the Date Jar
        </p>
      </p>
      <h3>Step 3</h3>
      <p>Figure out the logistics around planning the chosen date</p>
      <h3>Step 4</h3>
      <p>Enjoy date!</p>
    </>
  );
}
