import Image from "next/image";
import Link from "next/link";
import { useState } from "react";
import Modal from "../components/Modal";
import Instructions from "./Instructions";

export default function Header() {
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
  const [shouldFadeOut, setShouldFadeOut] = useState<boolean>(false);

  const closeModal = () => {
    setShouldFadeOut(true);
    setTimeout(() => {
      setIsModalVisible(false);
      setShouldFadeOut(false);
    }, 290);
  };

  return (
    <>
      <Link href="/">
        <Image
          src="/datejarlogo.png"
          width={200}
          height={125}
          alt="date jar logo"
        />
      </Link>
      <nav>
        <ul>
          <li>
            <Link href="/">Home</Link>
          </li>
          <li>
            <Link href="/about">About</Link>
          </li>
          <li>
            <button
              className="instruction-button"
              onClick={() => setIsModalVisible(true)}
            >
              Instructions
            </button>
          </li>
          <li>
            <Link href="/dateform">Add date idea!</Link>
          </li>
        </ul>
      </nav>
      {isModalVisible && (
        <Modal
          closeModal={closeModal}
          shouldFadeOut={shouldFadeOut}
          modalClassName="instruction-modal-container"
        >
          <Instructions />
        </Modal>
      )}
    </>
  );
}
