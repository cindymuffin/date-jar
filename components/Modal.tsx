import { Children, useState } from "react";
import { Date } from "../pages/index";
import DateResult from "./DateResult";

type ModalProp = {
  closeModal: () => void;
  shouldFadeOut: boolean;
  children: React.ReactNode;
  modalClassName?: string;
};

export default function Modal({
  closeModal,
  shouldFadeOut,
  children,
  modalClassName,
}: ModalProp) {
  return (
    <>
      <div
        className={
          !shouldFadeOut
            ? "modal-background"
            : "modal-background modal-fade-out"
        }
        onClick={closeModal}
      ></div>
      <div
        className={`${
          !shouldFadeOut ? "modal-container" : "modal-container modal-fade-out"
        } ${modalClassName ?? ""}`}
      >
        <div className="modal-close-button">
          <button onClick={closeModal}>&times;</button>
        </div>
        {children}
      </div>
    </>
  );
}
