import React from "react";
import { Date } from "../pages";

type DateResultProp = {
  dateResult: Date | null;
};

export default function DateResult({ dateResult }: DateResultProp) {
  return (
    <div>
      <p>Our date will be...</p>
      <h1>{dateResult?.activity}</h1>
      <p>Woohoo!</p>
    </div>
  );
}
