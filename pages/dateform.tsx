import { ChangeEvent, FormEvent, useState } from "react";
import Header from "../components/Header";
import { Date } from "./index";

export default function DateForm() {
  const [formData, setFormData] = useState<Date>({
    dateType: "",
    cost: "",
    activity: "",
  });

  const handleDateTypeChange = (event: ChangeEvent<HTMLInputElement>) => {
    console.log(event);
    setFormData({ ...formData, dateType: event.target.value });
  };

  const handleCostChange = (event: ChangeEvent<HTMLInputElement>) => {
    setFormData({ ...formData, cost: event.target.value });
  };

  const handleActivityChange = (event: ChangeEvent<HTMLInputElement>) => {
    setFormData({ ...formData, activity: event.target.value });
  };

  function handleSubmit(event: FormEvent<HTMLFormElement>) {
    event.preventDefault();

    const localStorageDateIdeas = JSON.parse(
      localStorage.getItem("dates") ?? "[]"
    );
    localStorageDateIdeas.push(formData);
    localStorage.setItem("dates", JSON.stringify(localStorageDateIdeas));

    const cleared: Date = {
      dateType: "",
      cost: "",
      activity: "",
    };
    setFormData(cleared);
  }

  return (
    <>
      <div className="body">
        <Header />
        <div className="form-box">
          <h1>Add Date Idea</h1>
          <form onSubmit={handleSubmit} id="add-date-form">
            <div className="date-type">
              <label htmlFor="dateType">Select Date Type</label>
              <div className="radio-container">
                <input
                  onChange={handleDateTypeChange}
                  value="in"
                  type="radio"
                  name="date-type"
                  id="date-type-in"
                  checked={formData.dateType === "in"}
                  className="form-input"
                />
                <label
                  htmlFor="date-type-in"
                  className={formData.dateType === "in" ? "selected-type" : ""}
                >
                  Stay home
                </label>
                <input
                  onChange={handleDateTypeChange}
                  value="out"
                  type="radio"
                  name="date-type"
                  id="date-type-out"
                  checked={formData.dateType === "out"}
                  className="form-input"
                />
                <label
                  htmlFor="date-type-out"
                  className={formData.dateType === "out" ? "selected-type" : ""}
                >
                  Go out
                </label>
              </div>
            </div>
            <div className="cost-type">
              <label htmlFor="cost">Select Cost</label>
              <div className="radio-container">
                <input
                  onChange={handleCostChange}
                  value="affordable"
                  type="radio"
                  name="cost"
                  id="cost-affordable"
                  checked={formData.cost === "affordable"}
                  className="form-input"
                />
                <label
                  htmlFor="cost-affordable"
                  className={
                    formData.cost === "affordable" ? "selected-type" : ""
                  }
                >
                  Affordable
                </label>
                <input
                  onChange={handleCostChange}
                  value="splurge"
                  type="radio"
                  name="cost"
                  id="cost-splurge"
                  checked={formData.cost === "splurge"}
                  className="form-input"
                />
                <label
                  htmlFor="cost-splurge"
                  className={formData.cost === "splurge" ? "selected-type" : ""}
                >
                  Splurge
                </label>
              </div>
            </div>
            <div className="activity">
              <label htmlFor="activity">Activity</label>
              <input
                onChange={handleActivityChange}
                value={formData.activity}
                placeholder="Enter activity here"
                required
                name="activity"
                id="activity"
                className="form-input"
              />
            </div>
            <button className="date-button">Add Date!</button>
          </form>
        </div>
      </div>
    </>
  );
}
