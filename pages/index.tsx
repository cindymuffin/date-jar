import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import { ChangeEvent, useState } from "react";
import Modal from "../components/Modal";
import DateResult from "../components/DateResult";
import Header from "../components/Header";

export type Date = {
  dateType: string;
  cost: string;
  activity: string;
};

export default function Home() {
  const [dateType, setDateType] = useState<string | null>(null);
  const [cost, setCost] = useState<string | null>(null);
  const [dateResult, setDateResult] = useState<Date | null>(null);
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
  const [shouldFadeOut, setShouldFadeOut] = useState<boolean>(false);

  const handleDateTypeChange = (event: ChangeEvent<HTMLInputElement>) => {
    setDateType(event.target.value);
  };

  const handleCostChange = (event: ChangeEvent<HTMLInputElement>) => {
    setCost(event.target.value);
  };

  const dateRandomizer = () => {
    const dateIdeas: Date[] = JSON.parse(localStorage.getItem("dates") ?? "[]");

    const dateIdeaFilter = dateIdeas.filter((date) => {
      return date.dateType === dateType && date.cost === cost;
    });

    const randomizedDate =
      dateIdeaFilter[Math.floor(Math.random() * dateIdeaFilter.length)];

    setDateResult(randomizedDate);

    setIsModalVisible(true);
  };

  const luckyRandomizer = () => {
    const dateIdeas: Date[] = JSON.parse(localStorage.getItem("dates") ?? "[]");

    const randomizedDate =
      dateIdeas[Math.floor(Math.random() * dateIdeas.length)];

    setDateResult(randomizedDate);

    setIsModalVisible(true);
  };

  const closeModal = () => {
    setShouldFadeOut(true);
    setTimeout(() => {
      setIsModalVisible(false);
      setShouldFadeOut(false);
    }, 290);
  };

  return (
    <>
      <div className="body">
        <Header />
        <div className="preference-box">
          <h1>Date Preferences</h1>
          <div>
            <h2>Would we rather...</h2>
            <div className="radio-container preferences-radio-container">
              <input
                type="radio"
                id="in"
                value="in"
                checked={dateType === "in"}
                onChange={handleDateTypeChange}
              />
              <label
                htmlFor="in"
                className={dateType === "in" ? "selected-type" : ""}
              >
                Stay home
              </label>
              <input
                type="radio"
                id="out"
                value="out"
                checked={dateType === "out"}
                onChange={handleDateTypeChange}
              />
              <label
                htmlFor="out"
                className={dateType === "out" ? "selected-type" : ""}
              >
                Go out
              </label>
            </div>
          </div>
          <div>
            <h2>The cost of our date will be...</h2>
            <div className="radio-container preferences-radio-container">
              <input
                type="radio"
                id="affordable"
                value="affordable"
                checked={cost === "affordable"}
                onChange={handleCostChange}
              />
              <label
                htmlFor="affordable"
                className={cost === "affordable" ? "selected-type" : ""}
              >
                Affordable
              </label>
              <input
                type="radio"
                id="splurge"
                value="splurge"
                checked={cost === "splurge"}
                onChange={handleCostChange}
              />
              <label
                htmlFor="splurge"
                className={cost === "splurge" ? "selected-type" : ""}
              >
                Splurge
              </label>
            </div>
          </div>
          <div>
            <button
              className="date-button"
              onClick={dateRandomizer}
              disabled={!dateType || !cost}
            >
              Lets go on a date!
            </button>
          </div>
          <div>
            <button
              className="date-button lucky-button"
              onClick={luckyRandomizer}
            >
              We're feelin' lucky~
            </button>
          </div>
          {isModalVisible && (
            <Modal closeModal={closeModal} shouldFadeOut={shouldFadeOut}>
              <DateResult dateResult={dateResult} />
            </Modal>
          )}
        </div>
      </div>
    </>
  );
}
