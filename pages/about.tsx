import Header from "../components/Header";

export default function About() {
  return (
    <>
      <div className="body">
        <Header />
        <div className="about-box">
          <h1>About</h1>
          <p>
            The idea of the Date Jar app came from seeing couples physically
            make date jars on Tiktok. My husband and I were about to make our
            very own date jar when he said, "You know, you could make this into
            an app." to encourage me to utilize my newly developed software
            engineering skills and alas, here is said app!
          </p>
          <h3>What is a date jar?</h3>
          <p>
            A date jar is a jar filled with either pieces of folded paper or
            popsicle sticks with date ideas written on them and couples will
            randomly pick from the jar for their next date idea. While any date
            could be randomly chosen from the jar, I liked the idea of
            separating dates into different categories which couples on Tiktok
            had done through using different colors for different categories.
          </p>
          <p>
            It made sense to me to differentiate between staying at home or
            going out for a date as well as the cost of the date because some
            days you and your partner may just feel like doing something lowkey
            where as other days you may want something more fancy (or maybe even
            something inbetween the two)!
          </p>
          <h3>How does Date Jar work?</h3>
          <p>
            Date Jar uses local storage to store all of your date ideas on the
            device you use. This means you and your partner will have to use ONE
            device to enter in date ideas and you will have to use the same
            device to generate your randomly chosen date.
          </p>
        </div>
      </div>
    </>
  );
}
